(declaim (optimize safety debug))

(eval-when (:compile-toplevel)
    (ql:quickload '(let-plus)))

;; TODO: Simplify the (eval-when):
(eval-when (:compile-toplevel :load-toplevel)
  (unless (find-package :cllib)
    (let* ((wd (make-pathname :directory (pathname-directory (or *compile-file-pathname* *load-pathname*))))
           ;; TODO: Why can't I just write "fileio" here?:
           (fileio (merge-pathnames "fileio.lisp" wd)))
      (assert (probe-file wd))
      (if (probe-file fileio)
          (load fileio)
          (error "In util.lisp: fileio.lisp not found.  Check your working directory.")))))

;;; TODO: Make sure (load-compile-maybe) creates the .fasl if it DNE:
;;; TODO: Make sure it compiles if the .fasl is older:
#| The semantics that I desire are:
  if fasl DNE or is newer than file
     compile file
  end
  load fasl

Include a parameter keyword: (verbose *load-verbose*)
|#

#|
(defpackage :util
  (:use :common-lisp :let-plus :cllib)
  #+SBCL (:shadowing-import-from :sb-posix
                                 #:getcwd
                                 #:chdir)
  #+SBCL (:export #:getcwd
                  #:chdir)
  (:export
   #:typename
   #:let+
   #:let-1
   #:let+1
   #:λ
   #:listify
   #:method-named?
   #:remove-method-named!
   #:obliterate-symbol
   #:optional
   #:make-package-maybe
   #:load-compile-file
   #:equal?)
  ;; fileio (cllib):
  (:export
   #:load-compile-maybe))
|#

;;;; Alternative to (defpackage), allowing more specific control of symbolling:
(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :util)
    (make-package :util
                  :use '(:common-lisp :let-plus :cllib))))

(in-package :util)

(eval-when (:compile-toplevel :load-toplevel :execute)
  #+SBCL (dolist (s '(sb-posix:getcwd
                      sb-posix:chdir))
           (shadowing-import s)
           (export s))
  (export '(typename
            let-1
            let+1
            λ
            listify
            method-named?
            remove-method-named!
            obliterate-symbol
            optional
            make-package-maybe
            load-compile-file
            equal?))
  ;; Export everything from let-plus:
  (do-external-symbols (s :let-plus)
    (shadowing-import s)
    (export s)))

(deftype typename ()
  '(or (and symbol (not null)) cons))

(defmacro let-1 (var binding &body body)
  `(let ((,var ,binding))
     ,@body))

(defmacro let+1 (var binding &body body)
  `(let+ ((,var ,binding))
     ,@body))

(defmacro λ (parms &body body)
  "My standard lambda-simplifier macro."
  ;; TODO: Using (lambda+) here caused some macroexpansion errors with &rest keys:
  `(lambda ,(etypecase parms
               (list parms)
               (symbol (list parms)))
     ,@body))

(defun listify (obj)
  (typecase obj
    (list obj)
    (t (list obj))))

(defun listify-specializers (s)
  "Internal util func; wraps object in a list."
  (etypecase s
    (cons
     s)
    ((and symbol (not null))
     (list s))))

(defun get-qualifiers-and-specializers (quals-and-specs)
  "Internal util func; returns a two-element list."
  (let (qualifiers specializers)
    (cond
      ((= 1 (length quals-and-specs))
       (setf qualifiers ())
       (setf specializers (listify-specializers (first quals-and-specs))))
      (t
       (setf qualifiers (etypecase (first quals-and-specs)
                          (keyword
                           (list (first quals-and-specs)))
                          (list
                           (first quals-and-specs))))
       (setf specializers (listify-specializers (second quals-and-specs)))))
    (check-type qualifiers list)
    (check-type specializers list)
    (list qualifiers specializers)))

(defmacro method-named? (name &rest quals-and-specs)
  "Synsugar macro.  Returns t if a matching method exists."
  (let+1 (qualifiers specializers) (get-qualifiers-and-specializers quals-and-specs)
    (declare (list qualifiers specializers))
    `(when (find-method (function ,name) ',qualifiers ',specializers nil) t)))

(defmacro remove-method-named! (name &rest quals-and-specs)
  "Utility for excising a particular multimethod specification.  Error if no such method exists.
NB: Do not quote the name, qualifiers, or specializations.  E.g., these are valid uses:
  (remove-method-named! any-func) ; Like using (fmakunbound)
  (remove-method-named! unary-func someclass)
  (remove-method-named! binary-func (class1 class2))
  (remove-method-named! initialize-instance :after someclass)"
  (declare (symbol name))
  (typecase quals-and-specs
    (null `(if (fboundp ',name)
               (fmakunbound ',name)
               (error "Error in (remove-method-named!): ~A does not seem to name a function." ',name)))
    (t (let+1 (qualifiers specializers) (get-qualifiers-and-specializers quals-and-specs)
       (declare (list qualifiers specializers))
       `(remove-method (function ,name)
                       (find-method (function ,name) ',qualifiers ',specializers))))))

(defun obliterate-symbol (s)
  "Remove all the bindings I can think of."
  (declare (symbol s))
  (makunbound s)
  (fmakunbound s)
  (unexport s)
  (unintern s))

(deftype optional (type)
  "Like Haskell's 'Maybe'."
  (assert (not (member type '(null *))))
  `(or null ,type))

(defun make-package-maybe (package-descriptor &rest make-package-keys)
  "Make package unless it already exists, in which case do nothing and return nil."
  (declare (type (or symbol string) package-descriptor))
  (unless (find-package package-descriptor)
    (apply #'make-package package-descriptor make-package-keys)))

(defun load-compile-file (file &rest keys &key verbose)
  "When given a filename without an extension, load the .lisp version, then compile it, then reload the compiled version."
  (declare (ignorable verbose))
  (let-1 file-extension (pathname-type file)
    (cond
      ((null file-extension)
       (apply #'load-compile-file (make-pathname :name file :type "lisp") keys))
      ((string-equal file-extension "lisp")
       (unless (probe-file file) (error "(load-compile-file): Can't find ~A!" file))
       (apply #'load file keys #|(when verbose `(:verbose ,verbose))|#)
       ;; TODO: Compare .fasl newness
       (compile-file file)
       (let+ ((filename (pathname-name file))
              (fasl (make-pathname :name filename :type "fasl")))
         (assert (probe-file fasl))
         (load fasl)))
      ((string-equal file-extension "fasl")
       (apply #'load file keys))
      ('otherwise
       (error "Invalid filename arg (load-compile-file ~S)." file)))))

(declaim (inline equal?))
(defmethod equal? (x y)
  "Generic equality."
  (equalp x y))

#| TODO: (load-compile-file):
  If passed a filename w/o extension
   Call self with .lisp extension added
  Otherwise:
   Load arg't
   Compile arg't
   Load resultant .fasl
|#

#| TODO: PROMOTING|DEMOTING (LET) FORMS:
Here are some logical rules to implement:
  * (let), (let*) demote to (let-1) iff only one binding is declared and initialized
  * (let+) demotes to (let+1) iff only one binding is declared and initialized
  * (let-1) promotes to (let*) if its first body-form is a (let), (let*), or (let-1)
  * (let+1) promotes to (let+) if its first body-form is any species of (let)
  * (let), (let*), (let-1), promote to (let+) if its first body-form is (let+)
  * (let+) promotes to a larger (let+) if its first body-form is any species of (let)
|#

;; TODO: (shadowing-import-from)
